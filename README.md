-------------
UseCase1
-------------

Task I.
Develop containerized app running web server and serving your Online CV.
Let's call such app myCV.
Bundle your web page code into Docker Image and store it into Docker HUB.

Steps:


deploy aCloudGuru Playground VM as follows:
hostname: builder
OS: centos7
VM_size: micro


add extra HDD of capacity ~1-2 GB and attach it to your VM


from an extra HDD create create:
VG: 		data / 1GB
LV: 		docker / 500MB
LV filesystem: 	ext4
mountpoint for LV: 	/var/lib/docker


ensure that Docker Engine is installed and running, must survive reboot


create build directory /build which will hold your code


develop index.html containing your real data
Use the following code as example:
https://github.com/ritaly/HTML-CSS-CV-demo


store your code into GitLab, name your project "mycv"


create Docker Image containing your code + web server
Name of Image: myCV:final
Use: nginx:stable as Base for your Image


use your account within Docker Hub and push your Image in to it


test your doings by creating Docker Container from your Image on your VM
Name of container: my-cv
Expose port: 80


Create local user instructor within group devops
user: instructor
uid: 666
password: Passw0rd



    group: devops
    gid: 666
solution (11):

getent group | grep devops
groupadd -g 666 devops
useradd instructor -u 666 -g 666 -m -s /bin/bash
passwd instructor

--------------
UseCase 2
--------------
Task II.
Implement GitLab CI/CD Pipeline.

Steps:

develop a simple GitLab CI/CD Pipeline which will automate the process of building and pushing your Image into your Docker HUB registry in case that content of index.html has changed.
stages:

build




   
    jobs:
    build_job

Task III.
Deploy real K8S cluster using kubeadm.

Steps:


deploy 3 VMs using aCloudGuru playground
OS: Ubuntu 20.04 LTS
Image: 2xCPU + 4GB RAM
Hostnames:

master1

worker1

worker3




perform OS update on all 3 machines


Create local user instructor within group devops on all 3 VMs
user: instructor
uid: 666
password: Passw0rd



    group: devops
    gid: 666


Install K8S (1x master + 2x worker nodes)
Follow the official docu for detailed installation steps for a particular distro:
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/


Deploy your first APP

create deployment named mycv with 3 replicas from Image you have stored in DockerHub
expose your deployment as service named mycv-svc, ports=80:80, type=NodePort



on master1 install & start httpd service, store your email address into \
/var/www/html/index.html


Check:

please provide your Public IP build machine & master1
