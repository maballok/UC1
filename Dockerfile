FROM nginx:stable
LABEL maintainer="marian.ballok@gmail.com"
COPY ./index.html /usr/share/nginx/html
COPY ./style.css  /usr/share/nginx/html
COPY ./img /usr/share/nginx/html/img
CMD ["nginx", "-g", "daemon off;"]
EXPOSE 80

